<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one or more
  ~ contributor license agreements.  See the NOTICE file distributed with
  ~ this work for additional information regarding copyright ownership.
  ~ The ASF licenses this file to You under the Apache License, Version 2.0
  ~ (the "License"); you may not use this file except in compliance with
  ~ the License.  You may obtain a copy of the License at
  ~
  ~    http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

## Named Entity Recognition (Cogito)

<p align="center"> 
    <img src="icon.png" width="150px;" class="pe-image-documentation"/>
</p>

***

## Description

Annotation of entities: People, Organizations, Places, Known concepts, Unknown concepts. And also tags: urls, mail 
addresses, phone numbers, addresses, dates, time, measures, money, percentage, file folder.

For more details, see: https://live.european-language-grid.eu/catalogue/tool-service/474

***

## Required input

Text field to apply NER. Supported languages:
* Italian
* Portuguese
* Dutch/Flemish
* Spanish/Castilian 
* English
* French
* German

***

## Output

Named entities in English.