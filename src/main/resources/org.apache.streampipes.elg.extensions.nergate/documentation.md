<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one or more
  ~ contributor license agreements.  See the NOTICE file distributed with
  ~ this work for additional information regarding copyright ownership.
  ~ The ASF licenses this file to You under the Apache License, Version 2.0
  ~ (the "License"); you may not use this file except in compliance with
  ~ the License.  You may obtain a copy of the License at
  ~
  ~    http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

## Named Entity Recognition (Gate)

<p align="center"> 
    <img src="icon.png" width="150px;" class="pe-image-documentation"/>
</p>

***

## Description

A named entity recognition service for documents. Based on ANNIE, it identifies names of `persons`, 
`locations`, and `organizations`. For tweets, it performs normalization of abbreviations and common Twitter slang.

For more details, see: 
* English: https://live.european-language-grid.eu/catalogue/tool-service/538
* German: https://live.european-language-grid.eu/catalogue/tool-service/512
* German (Tweets): https://live.european-language-grid.eu/catalogue/tool-service/513  
* French: https://live.european-language-grid.eu/catalogue/tool-service/508
* French (Tweets): https://live.european-language-grid.eu/catalogue/tool-service/509

***

## Required input

Text field to apply NER. Supported languages based on selected option:
* English
* German
* French

***

## Configurations

Select language model option.

## Output

Appends a list of recognized named entities to the input event.