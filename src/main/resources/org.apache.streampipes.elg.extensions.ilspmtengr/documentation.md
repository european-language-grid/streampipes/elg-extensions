<!--
  ~ Licensed to the Apache Software Foundation (ASF) under one or more
  ~ contributor license agreements.  See the NOTICE file distributed with
  ~ this work for additional information regarding copyright ownership.
  ~ The ASF licenses this file to You under the Apache License, Version 2.0
  ~ (the "License"); you may not use this file except in compliance with
  ~ the License.  You may obtain a copy of the License at
  ~
  ~    http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

## Machine Translation (ILSP)

<p align="center"> 
    <img src="icon.png" width="150px;" class="pe-image-documentation"/>
</p>

***

## Description

A machine translation (MT) tool for English-to-Greek based on the Marian machine translation framework. The 
translation model is a basic transformer model trained over a Greek-English corpus of 10.7M sentence pairs using 
Marian NMT.

For more details, see: https://live.european-language-grid.eu/catalogue/tool-service/479

***

## Required input

Text field containing text in English language.

***

## Output

Appended machine translated text in Modern Greek.