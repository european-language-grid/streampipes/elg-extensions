/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.streampipes.elg.model;

import org.apache.streampipes.model.runtime.field.ListField;
import org.apache.streampipes.model.runtime.field.NestedField;

import java.util.List;

public class NERModel {

    private List<NEREntity> entities;

    public NERModel(List<NEREntity> entities) {
        this.entities = entities;
    }

    public NERModel() {
    }

    public List<NEREntity> getEntities() {
        return entities;
    }

    public void setEntities(List<NEREntity> entities) {
        this.entities = entities;
    }

    public ListField toSpEvent() {
        ListField list = new ListField("annotations");

        if (this.getEntities().size() > 0) {
            this.getEntities().forEach(entity -> {
                NestedField tempResult = new NestedField();
                tempResult.addField("start", entity.getStart());
                tempResult.addField("end", entity.getEnd());
                tempResult.addField("entityType", entity.getEntityType());
                if (entity.getEntityValue() == null) {
                    entity.setEntityValue("n/a");
                }
                tempResult.addField("entityValue", entity.getEntityValue());
                list.add(tempResult);
            });
        }
        return list;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NERModel{entities=");
        entities.forEach(entity -> builder.append(entity.toString()).append(","));
        builder.append("}");
        return builder.toString();
    }
}
