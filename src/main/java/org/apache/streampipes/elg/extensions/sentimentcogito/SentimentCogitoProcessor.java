/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.streampipes.elg.extensions.sentimentcogito;

import com.google.gson.JsonObject;
import org.apache.streampipes.elg.config.ConfigKeys;
import org.apache.streampipes.elg.extensions.AbstractElgProcessor;
import org.apache.streampipes.commons.exceptions.SpRuntimeException;
import org.apache.streampipes.model.DataProcessorType;
import org.apache.streampipes.model.graph.DataProcessorDescription;
import org.apache.streampipes.model.runtime.Event;
import org.apache.streampipes.model.schema.PropertyScope;
import org.apache.streampipes.sdk.builder.ProcessingElementBuilder;
import org.apache.streampipes.sdk.builder.StreamRequirementsBuilder;
import org.apache.streampipes.sdk.helpers.*;
import org.apache.streampipes.sdk.utils.Assets;
import org.apache.streampipes.svcdiscovery.api.SpConfig;
import org.apache.streampipes.wrapper.context.EventProcessorRuntimeContext;
import org.apache.streampipes.wrapper.routing.SpOutputCollector;
import org.apache.streampipes.wrapper.standalone.ProcessorParams;

public class SentimentCogitoProcessor extends AbstractElgProcessor {

    private static final String ENDPOINT_APPENDIX = "cogsenti";

    private static final String TEXT_FIELD_KEY = "text-field";
    private static final String SENTIMENT_FIELD = "sentiment-field";
    private static final String SENTIMENT_PROPERTY = "sentiment";

    private String textFieldSelector;
    private String elgApiKey;

    @Override
    public DataProcessorDescription declareModel() {
        return ProcessingElementBuilder.create("org.apache.streampipes.elg.extensions.sentimentcogito")
                .category(DataProcessorType.ENRICH_TEXT)
                .withAssets(Assets.DOCUMENTATION, Assets.ICON)
                .withLocales(Locales.EN)
                .requiredStream(StreamRequirementsBuilder
                        .create()
                        .requiredPropertyWithUnaryMapping(
                                EpRequirements.stringReq(),
                                Labels.withId(TEXT_FIELD_KEY),
                                PropertyScope.NONE)
                        .build())
                .outputStrategy(OutputStrategies.append(
                        EpProperties.doubleEp(
                                Labels.withId(SENTIMENT_FIELD),
                                SENTIMENT_PROPERTY,
                        "http://schema.org/sentiment")))
                .build();

    }

    @Override
    public void onInvocation(ProcessorParams params, SpOutputCollector out,
                             EventProcessorRuntimeContext ctx) throws SpRuntimeException {
        // extract selected text field
        this.textFieldSelector = params.extractor().mappingPropertyValue(TEXT_FIELD_KEY);

        SpConfig config = ctx.getConfigStore().getConfig();
        this.elgApiKey = config.getString(ConfigKeys.ELG_API_KEY);
    }

    @Override
    public void onEvent(Event event, SpOutputCollector out) throws SpRuntimeException {

        // extract text
        String text = event.getFieldBySelector(textFieldSelector).getAsPrimitive().getAsString();

        // Call ELG component
        JsonObject response = performElgRequestToJson(elgApiKey, ENDPOINT_APPENDIX, text, "features", false);
        Double sentiment = extractSentiment(response);

        // map to event model and output
        event.addField(SENTIMENT_PROPERTY, sentiment);
        out.collect(event);
    }

    @Override
    public void onDetach() throws SpRuntimeException {
        // do nothing
    }

    // Helpers

    public Double extractSentiment(JsonObject jsonObject) {
        return jsonObject.get("OVERALL").getAsDouble();
    }

}
