/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.streampipes.elg.extensions;

import com.google.gson.*;
import org.apache.http.Consts;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.apache.streampipes.commons.exceptions.SpRuntimeException;
import org.apache.streampipes.wrapper.standalone.StreamPipesDataProcessor;

import java.io.IOException;

public abstract class AbstractElgProcessor extends StreamPipesDataProcessor {

    private static final String ELG_URI = "https://live.european-language-grid.eu/execution/processText/";

    public String performElgRequest(String elgApiKey, String endpointAppendix, String text) throws IOException {

        ResponseHandler<String> myHandler = response -> EntityUtils.toString(response.getEntity(), Consts.UTF_8);

        return Request.Post(ELG_URI + endpointAppendix)
                .addHeader("Content-type", "text/plain; charset=UTF-8")
                .addHeader("Authorization", makeAuthHeader(elgApiKey))
                .bodyString(text, ContentType.TEXT_PLAIN)
                .execute()
                .handleResponse(myHandler);
    }

    public JsonObject performElgRequestToJson(String elgApiKey,
                                              String endpointAppendix,
                                              String text,
                                              String topLevelObject,
                                              boolean isJsonArray) throws SpRuntimeException {
        try {
            String response = performElgRequest(elgApiKey, endpointAppendix, text);
            return parseAnnotations(response, topLevelObject, isJsonArray);
        } catch (IOException e) {
            throw new SpRuntimeException("Could not get ELG response");
        }
    }

    protected JsonObject parseAnnotations(String response, String topLevelObject, boolean isJsonArray) {
        JsonObject obj = new JsonObject();
        if (isJsonArray) {
            JsonArray arr = JsonParser.parseString(response)
                    .getAsJsonObject()
                    .getAsJsonObject("response")
                    .getAsJsonArray(topLevelObject);

            obj.add("result", arr);
        } else {
            obj = JsonParser.parseString(response)
                    .getAsJsonObject()
                    .getAsJsonObject("response")
                    .getAsJsonObject(topLevelObject);
        }
        return obj;
    }

    private String makeAuthHeader(String elgApiKey) {
        return "Bearer " + elgApiKey;
    }
}
