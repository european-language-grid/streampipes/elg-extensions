/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.streampipes.elg.extensions.nercogito;

import com.google.gson.JsonObject;
import org.apache.streampipes.elg.config.ConfigKeys;
import org.apache.streampipes.elg.extensions.AbstractElgProcessor;
import org.apache.streampipes.commons.exceptions.SpRuntimeException;
import org.apache.streampipes.elg.model.NEREntity;
import org.apache.streampipes.elg.model.NERModel;
import org.apache.streampipes.model.DataProcessorType;
import org.apache.streampipes.model.graph.DataProcessorDescription;
import org.apache.streampipes.model.runtime.Event;
import org.apache.streampipes.model.schema.PropertyScope;
import org.apache.streampipes.sdk.builder.ProcessingElementBuilder;
import org.apache.streampipes.sdk.builder.StreamRequirementsBuilder;
import org.apache.streampipes.sdk.helpers.*;
import org.apache.streampipes.sdk.utils.Assets;
import org.apache.streampipes.svcdiscovery.api.SpConfig;
import org.apache.streampipes.wrapper.context.EventProcessorRuntimeContext;
import org.apache.streampipes.wrapper.routing.SpOutputCollector;
import org.apache.streampipes.wrapper.standalone.ProcessorParams;

import java.util.ArrayList;
import java.util.List;

public class NERCogitoProcessor extends AbstractElgProcessor {

    private static final String ENDPOINT_APPENDIX = "cogner";
    private static final String TEXT_FIELD_KEY = "text-field";

    private String selectedTextField;
    private String elgApiKey;

    @Override
    public DataProcessorDescription declareModel() {
        return ProcessingElementBuilder.create("org.apache.streampipes.elg.extensions.nercogito")
                .category(DataProcessorType.ENRICH_TEXT)
                .withAssets(Assets.DOCUMENTATION, Assets.ICON)
                .withLocales(Locales.EN)
                .requiredStream(StreamRequirementsBuilder
                        .create()
                        .requiredPropertyWithUnaryMapping(
                                EpRequirements.stringReq(),
                                Labels.withId(TEXT_FIELD_KEY),
                                PropertyScope.NONE)
                        .build())
                .outputStrategy(OutputStrategies.fixed(
                        EpProperties.timestampProperty("timestamp"),
                        EpProperties.integerEp(Labels.from("start", "Start", ""), "start", "http://elg.com/start"),
                        EpProperties.integerEp(Labels.from("end", "End", ""), "end", "http://elg.com/end"),
                        EpProperties.stringEp(Labels.from("entityType", "Entity Type", ""), "entityType", "http://elg.com/entityType"),
                        EpProperties.stringEp(Labels.from("entityValue", "Entity Value", ""), "entityValue", "http://elg.com/entityValue")))
                .build();

    }

    @Override
    public void onInvocation(ProcessorParams params, SpOutputCollector out,
                             EventProcessorRuntimeContext ctx) throws SpRuntimeException {

        // extract selected text field
        this.selectedTextField = params.extractor().mappingPropertyValue(TEXT_FIELD_KEY);

        SpConfig config = ctx.getConfigStore().getConfig();
        this.elgApiKey = config.getString(ConfigKeys.ELG_API_KEY);
    }

    @Override
    public void onEvent(Event event, SpOutputCollector out) throws SpRuntimeException {

        // extract text
        String text = event.getFieldBySelector(selectedTextField).getAsPrimitive().getAsString();
        // Call ELG component
        JsonObject response = performElgRequestToJson(elgApiKey, ENDPOINT_APPENDIX, text, "annotations", false);
        NERModel model = transformReponse(response);

        // map to event model and output
        model.getEntities().forEach(entity -> {
            Event outEvent = new Event();
            outEvent.addField("timestamp", System.currentTimeMillis());
            outEvent.addField("start", entity.getStart());
            outEvent.addField("end", entity.getEnd());
            outEvent.addField("entityType", entity.getEntityType());
            if (entity.getEntityValue() == null) {
                entity.setEntityValue("n/a");
            }
            outEvent.addField("entityValue", entity.getEntityValue());
            out.collect(outEvent);
        });
    }

    @Override
    public void onDetach() throws SpRuntimeException {
        // do nothing
    }

    // Helpers

    public NERModel transformReponse(JsonObject response) {
        List<NEREntity> entities = new ArrayList<>();
        response.getAsJsonObject().entrySet().forEach(entry -> {
            entry.getValue().getAsJsonArray().forEach(elem -> {
                NEREntity entity = new NEREntity();
                entity.setEntityType(entry.getKey());
                entity.setStart(elem.getAsJsonObject().get("start").getAsInt());
                entity.setEnd(elem.getAsJsonObject().get("end").getAsInt());
                entity.setEntityValue(elem.getAsJsonObject().get("features").getAsJsonObject().get("name").getAsString());

                entities.add(entity);
            });
        });
        return new NERModel(entities);
    }
}
