/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.streampipes.elg.extensions.nergate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.streampipes.elg.config.ConfigKeys;
import org.apache.streampipes.elg.extensions.AbstractElgProcessor;
import org.apache.streampipes.elg.model.LanguageModel;
import org.apache.streampipes.elg.model.NEREntity;
import org.apache.streampipes.elg.model.NERModel;
import org.apache.streampipes.commons.exceptions.SpRuntimeException;
import org.apache.streampipes.model.DataProcessorType;
import org.apache.streampipes.model.graph.DataProcessorDescription;
import org.apache.streampipes.model.runtime.Event;
import org.apache.streampipes.model.schema.PropertyScope;
import org.apache.streampipes.model.staticproperty.Option;
import org.apache.streampipes.sdk.builder.ProcessingElementBuilder;
import org.apache.streampipes.sdk.builder.StreamRequirementsBuilder;
import org.apache.streampipes.sdk.helpers.*;
import org.apache.streampipes.sdk.utils.Assets;
import org.apache.streampipes.svcdiscovery.api.SpConfig;
import org.apache.streampipes.wrapper.context.EventProcessorRuntimeContext;
import org.apache.streampipes.wrapper.routing.SpOutputCollector;
import org.apache.streampipes.wrapper.standalone.ProcessorParams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NERGateProcessor extends AbstractElgProcessor {

    private static final String TEXT_FIELD_KEY = "text-field";
    private static final String SELECT_LANGUAGE_KEY = "select-language-key";

    private final List<LanguageModel> supportedLanguages = Arrays.asList(
            new LanguageModel("English", "en", "gatenercen"),
            new LanguageModel("French", "fr", "gatenerfr"),
            new LanguageModel("French (Tweets)", "fr", "gatefrtweetner"),
            new LanguageModel("German", "de", "gategerner"),
            new LanguageModel("German (Tweets)", "de", "gategertweetner"));

    private String textFieldSelector;
    private String endpointAppendix;
    private String elgApiKey;

    @Override
    public DataProcessorDescription declareModel() {
        return ProcessingElementBuilder.create("org.apache.streampipes.elg.extensions.nergate")
                .category(DataProcessorType.ENRICH_TEXT)
                .withAssets(Assets.DOCUMENTATION, Assets.ICON)
                .withLocales(Locales.EN)
                .requiredStream(StreamRequirementsBuilder
                        .create()
                        .requiredPropertyWithUnaryMapping(
                                EpRequirements.stringReq(),
                                Labels.withId(TEXT_FIELD_KEY),
                                PropertyScope.NONE)
                        .build())
                .requiredSingleValueSelection(Labels.withId(SELECT_LANGUAGE_KEY), defaultLanguages())
                .outputStrategy(OutputStrategies.append(
                        EpProperties.listNestedEp(Labels.from("annotations", "Annotation", ""), "annotations",
                                Arrays.asList(
                                EpProperties.integerEp(Labels.from("start", "Start", ""), "start", "http://elg.com/start"),
                                EpProperties.integerEp(Labels.from("end", "End", ""), "end", "http://elg.com/end"),
                                EpProperties.stringEp(Labels.from("entityType", "Entity Type", ""), "entityType", "http://elg.com/entityType"),
                                EpProperties.stringEp(Labels.from("entityValue", "Entity Value", ""), "entityValue", "http://elg.com/entityValue")
                        ))))
                .build();
    }

    @Override
    public void onInvocation(ProcessorParams params, SpOutputCollector out,
                             EventProcessorRuntimeContext ctx) throws SpRuntimeException {
        // extract selected text field
        this.textFieldSelector = params.extractor().mappingPropertyValue(TEXT_FIELD_KEY);

        this.endpointAppendix = params.extractor().selectedSingleValueInternalName(SELECT_LANGUAGE_KEY, String.class);

        SpConfig config = ctx.getConfigStore().getConfig();
        this.elgApiKey = config.getString(ConfigKeys.ELG_API_KEY);
    }

    @Override
    public void onEvent(Event event, SpOutputCollector out) throws SpRuntimeException {
        // extract text
        String text = event.getFieldBySelector(this.textFieldSelector).getAsPrimitive().getAsString();

        // call ELG component
        JsonObject response = performElgRequestToJson(elgApiKey, endpointAppendix, text, "annotations", false);
        NERModel model = transformResponse(response);

        event.addField(model.toSpEvent());
        out.collect(event);
    }

    @Override
    public void onDetach() throws SpRuntimeException {
        // do nothing
    }

    // Helpers

    public NERModel transformResponse(JsonObject topObj) {
        List<NEREntity> nerEntities = new ArrayList<>();

        JsonArray persons = topObj.getAsJsonArray("Person");
        JsonArray locations = topObj.getAsJsonArray("Location");
        JsonArray organizations = topObj.getAsJsonArray("Organization");
        JsonArray dates = topObj.getAsJsonArray("Date");

        addElements(persons, nerEntities);
        addElements(locations, nerEntities);
        addElements(organizations, nerEntities);
        addElements(dates, nerEntities);

        return new NERModel(nerEntities);

    }

    private void addElements(JsonArray arr, List<NEREntity> entities) {
        if (arr.size() > 0) {
            arr.forEach(el -> {
                int start = el.getAsJsonObject().get("start").getAsInt();
                int end = el.getAsJsonObject().get("end").getAsInt();
                String rule = el.getAsJsonObject().get("features").getAsJsonObject().get("rule").getAsString();

                entities.add(new NEREntity(start, end, rule));
            });
        }
    }

    private List<Option> defaultLanguages() {
        return this.supportedLanguages.stream()
                .map(sl -> new Option(sl.getLabel(), sl.getEndpointAppendix()))
                .collect(Collectors.toList());
    }
}
