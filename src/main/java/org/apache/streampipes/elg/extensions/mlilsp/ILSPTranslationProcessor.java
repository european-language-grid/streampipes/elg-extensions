/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.apache.streampipes.elg.extensions.mlilsp;

import com.google.gson.JsonArray;
import org.apache.streampipes.commons.exceptions.SpRuntimeException;
import org.apache.streampipes.elg.config.ConfigKeys;
import org.apache.streampipes.elg.extensions.AbstractElgProcessor;
import org.apache.streampipes.model.DataProcessorType;
import org.apache.streampipes.model.graph.DataProcessorDescription;
import org.apache.streampipes.model.runtime.Event;
import org.apache.streampipes.model.schema.PropertyScope;
import org.apache.streampipes.sdk.builder.ProcessingElementBuilder;
import org.apache.streampipes.sdk.builder.StreamRequirementsBuilder;
import org.apache.streampipes.sdk.helpers.*;
import org.apache.streampipes.sdk.utils.Assets;
import org.apache.streampipes.svcdiscovery.api.SpConfig;
import org.apache.streampipes.wrapper.context.EventProcessorRuntimeContext;
import org.apache.streampipes.wrapper.routing.SpOutputCollector;
import org.apache.streampipes.wrapper.standalone.ProcessorParams;

public class ILSPTranslationProcessor extends AbstractElgProcessor {

    private static final String ENDPOINT_APPENDIX = "ilspmtengr";

    private static final String TRANSLATION_KEY = "translation-key";
    private static final String TEXT_FIELD_KEY = "text-field";
    private static final String TRANSLATED_TEXT = "translated-text";

    private String textFieldSelector;
    private String elgApiKey;

    @Override
    public DataProcessorDescription declareModel() {
        return ProcessingElementBuilder.create("org.apache.streampipes.elg.extensions.ilspmtengr")
                .category(DataProcessorType.ENRICH_TEXT)
                .withAssets(Assets.DOCUMENTATION, Assets.ICON)
                .withLocales(Locales.EN)
                .requiredStream(StreamRequirementsBuilder
                        .create()
                        .requiredPropertyWithUnaryMapping(
                                EpRequirements.stringReq(),
                                Labels.withId(TEXT_FIELD_KEY),
                                PropertyScope.NONE)
                        .build())
                .requiredSingleValueSelection(
                        Labels.withId(TRANSLATION_KEY),
                        Options.from("EN->GR"))
                .outputStrategy(OutputStrategies.append(
                        EpProperties.stringEp(
                                Labels.withId(TRANSLATED_TEXT),
                                "translation",
                                "http://schema.org/text")))
                .build();
    }

    @Override
    public void onInvocation(ProcessorParams params, SpOutputCollector out,
                             EventProcessorRuntimeContext ctx) throws SpRuntimeException {
        // extract selected text field
        this.textFieldSelector = params.extractor().mappingPropertyValue(TEXT_FIELD_KEY);

        SpConfig config = ctx.getConfigStore().getConfig();
        this.elgApiKey = config.getString(ConfigKeys.ELG_API_KEY);
    }

    @Override
    public void onEvent(Event event, SpOutputCollector out) throws SpRuntimeException {
        // extract text
        String text = event.getFieldBySelector(textFieldSelector).getAsPrimitive().getAsString();

        // call ELG component
        JsonArray resultArray =
                performElgRequestToJson(elgApiKey, ENDPOINT_APPENDIX, text, "texts", true).get("result").getAsJsonArray();

        if (resultArray != null && resultArray.size() > 0) {
            String translation = translate(resultArray);

            event.addField("translation", translation);
            out.collect(event);
        }
    }

    @Override
    public void onDetach() throws SpRuntimeException {
        // do nothing
    }

    // Helpers

    private String translate(JsonArray array) {
        return array.get(0).getAsJsonObject().get("content").getAsString();
    }
}
