# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM maven:3-jdk-8
WORKDIR /build
COPY * /build/
RUN mvn clean package javadoc:aggregate

#RUN export MVN_VERSION=$(mvn -f pom.xml org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dmaven.artifact.threads=1 -T 1 -X #-e -Dexpression=project.version -q -DforceStdout)

FROM adoptopenjdk/openjdk8-openj9:alpine
MAINTAINER dev@streampipes.apache.org

ENV CONSUL_LOCATION consul
EXPOSE 8090
COPY --from=0 /build/target/streampipes-elg-extensions.jar  /streampipes-elg-extensions.jar

ENTRYPOINT ["java", "-jar", "/streampipes-elg-extensions.jar"]